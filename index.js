const fs = require('fs');
const path = require('path');
const crypto = require('crypto');

const i18n = require('i18n-2');
const nunjucks = require('nunjucks');
const logger = require('morgan');
const bodyParser = require('body-parser');
const MemoryCache = require('memory-cache').Cache;

function getDefaultLocale(locales, req, fallback) {
    for (let i = 0; i < locales.length; i++) {
        if (req.acceptsLanguages(locales[i])) {
            return locales[i];
        }
    }

    return fallback;
}

module.exports.locales = function(app, {
    locales = ['fr', 'en'],
    defaultLocale = 'fr',
    directory,
    devMode = false,
}) {
    i18n.expressBind(app, {
        locales,
        directory,
        devMode,
    });

    app.use(function(req, res, next) {
        let urlLocale = req.path.split('/')[1];

        // Always force the locale to be in the url
        if (urlLocale.length && locales.indexOf(urlLocale) > -1) {
            locale = urlLocale;

            req.i18n.setLocale(locale);
            res.locals.locale = locale;
            res.locals.currentPath = req.path[req.path.length - 1] === '/' ? req.path.slice(0, -1) : req.path;
            res.locals.languageLinks = {};
            res.locals.currentYear = String((new Date()).getFullYear());

            for (let i = 0; i < locales.length; i++) {
                let urlArray = req.path.split('/');
                urlArray[1] = locales[i];
                const href = urlArray.join('/');

                res.locals.languageLinks[locales[i]] = {
                    href,
                    current: locales[i] === locale,
                };
            }

            next();
        } else {
            const locale = getDefaultLocale(locales, req, defaultLocale);

            res.redirect(`/${locale}${req.path}`);
        }
    });
};

module.exports.views = function(app, {
    path,
}) {
    // view engine setup
    app.set('views', path);
    app.set('view engine', 'njk');
    nunjucks.configure('views', {
        autoescape: true,
        express: app
    });
};

module.exports.errors = function(app) {
    // 404
    app.use(function(req, res, next) {
        const err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // Error handler
    app.use(function(err, req, res, next) {
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'dev' ? err : {};

        res.status(err.status || 500);

        if (req.accepts('html')) {
            res.render('error');
        } else {
            res.send({ error: err.message, messages: err.messages });
        }
    });
};

function ensureDirectory(path, callback) {
    fs.access(path, function(error) {
        if (error) {
            fs.mkdir(path, function(error) {
                callback && callback(error);
            });
        } else {
            callback && callback();
        }
    });
}

module.exports.ensureDirectory = ensureDirectory;

function getRandomString() {
    return new Promise((resolve, reject) => {
        crypto.randomBytes(16, (error, buffer) => {
            if (error) {
                reject(error);
                return;
            }

            resolve(buffer.toString('hex'));
        });
    });
}

module.exports.getRandomString = getRandomString;

module.exports.StatusError = function StatusError(message, status) {
    const error = new Error(typeof message === 'string' ? message : 'general');
    error.status = status;
    error.messages = typeof message === 'string' ? [{ message }] : message;
    return error;
}

function writeToFile(string, uploadsFolder) {
    return new Promise((resolve, reject) => {
        if (!string) {
            return resolve(null);
        }

        const regexString = '^data:.+\/(png|jpe?g|pdf|wav|mp3|mp4);base64,';
        const execResult = (new RegExp(regexString, 'i')).exec(string);

        if (execResult) {
            return getRandomString().then((randomString) => {
                // It is a base64 data string
                const extension = execResult[1];
                const filename = `${randomString}.${extension}`;
                const outputPath = path.join(uploadsFolder, filename);
                const base64Data = string.replace((new RegExp(regexString, 'i')), '');

                fs.writeFile(outputPath, base64Data, 'base64', function(error) {
                    if (error) {
                        reject(error);
                        return;
                    }

                    resolve(outputPath);
                });
            });
        } else if (/uploads/i.test(string)) {
            resolve(null);
        } else {
            reject(StatusError('Invalid file format', 400));
        }
    });
}

module.exports.writeToFile = writeToFile;

module.exports.processFile = function processFile(attributeName, uploadsFolder) {
    return function processFilesMiddleware(req, res, next) {
        const fileString = req.body.data.attributes[attributeName];

        writeToFile(fileString, uploadsFolder).then((fileSrc) => {
            if (fileSrc) {
                req.body.data.attributes[attributeName] = fileSrc;
            }

            next();
        }).catch((error) => {
            console.log('error processing files', error);

            next(error);
        });
    };
}

module.exports.setup = function server(app, {
    limit = '20mb',
}) {
    app.use(logger('dev'));
    app.use(bodyParser.json({ limit }));
    app.use(bodyParser.urlencoded({ extended: false, limit }));
};

module.exports.directories = function setup(app, {
    staticDirectories = {},
    static = () => {},
    environments = ['dev'],
}) {
    if (environments.includes(process.env.NODE_ENV)) {
        const keys = Object.keys(staticDirectories);

        keys.forEach((key) => {
            const dirPath = staticDirectories[key];

            ensureDirectory(dirPath);

            app.use(key, static(dirPath));
        });
    }
};

module.exports.Cache = class Cache {
    constructor() {
        this._cache = new MemoryCache();
    }

    get (key, storeFunction) {
        return new Promise((resolve, reject) => {
            const response = {
                fromCache: false,
                value: null,
            };
            const value = this._cache.get(key);

            if (value) {
                response.fromCache = true;
                response.value = value;

                return resolve(response);
            }

            if (typeof storeFunction !== 'function') {
                return resolve(response);
            }

            storeFunction().then((result) => {
                this._cache.put(key, result);

                response.fromCache = false;
                response.value = result;

                return resolve(response);
            }).catch((error) => {
                return reject(error);
            });
        });
    }

    del(key) {
        this._cache.del(key);
    }

    clear() {
        this._cache.clear();
    }
};

module.exports.apiKey = function apiKey(apiKey) {
    return function(req, res, next) {
        const requestApiKey = req.headers['x-api-key'] || req.query['apiKey'];

        if (!requestApiKey || requestApiKey !== apiKey) {
            res.status(400).send({ message: 'invalid api key'});
        } else {
            next();
        }
    };
};
